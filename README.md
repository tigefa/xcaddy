# xcaddy [![pipeline status](https://gitlab.gnome.org/tigefa/xcaddy/badges/main/pipeline.svg)](https://gitlab.gnome.org/tigefa/xcaddy/-/commits/main)

this build `xcaddy` with dns plugin Cloudflare

```bash
xcaddy build --output caddy --with github.com/caddy-dns/cloudflare --with github.com/caddy-dns/digitalocean --with github.com/ueffel/caddy-brotli
```

